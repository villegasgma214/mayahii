package jdl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Jdl {

	public static void main(String[] args) throws IOException {
		while (true) {
			InputStreamReader obj = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(obj);
			String string = br.readLine();
			int v = 0;
			for (int i = 0; i < string.length(); i++) {
				v += Jdl.getChartValue(string.charAt(i));				
			}
			System.out.println(v);
		}
	}

	private static int getChartValue(char a) {
		if (a == 'P' || a == 'L') {
			return 0;
		}
		String sequenceqp = "QWERTYUIO";
		String sequenceal = "ASDFGHJKL";
		String sequencezm = "ZXCVBNM";
		if (sequenceqp.indexOf(Character.toString(a)) > -1) {
			return sequenceqp.indexOf(Character.toString(a)) + 1;
		}
		if (sequenceal.indexOf(Character.toString(a)) > -1) {
			return sequenceal.indexOf(Character.toString(a)) + 1;
		}
		if (sequencezm.indexOf(Character.toString(a)) > -1) {
			return sequencezm.indexOf(Character.toString(a)) + 1;
		}
		return 0;
	}

}
