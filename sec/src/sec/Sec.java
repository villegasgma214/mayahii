package sec;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Sec {
	public static void main(String[] args) throws NumberFormatException, IOException {	
		while(true) {
			double a, b, c;
			InputStreamReader obj = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(obj);		
			a = Double.parseDouble(br.readLine());
			b = Double.parseDouble(br.readLine());
			c = Double.parseDouble(br.readLine());
			Sec.calculoDeEcuaciónCuadratica(a, b, c);
		}
	}
	
	private static void calculoDeEcuaciónCuadratica(Double a, Double b, Double c) {
		Double x1, x2, discriminante;
		discriminante = (b * b) - (4 * a * c);
		if (discriminante == 0) {
			x1 = x2 = -b / (2 * a);
			System.out.println("1");
		} else if (discriminante > 0) {
			x1 = (b + Math.sqrt(discriminante)) / (2 * a);
			x2 = -(b + Math.sqrt(discriminante)) / (2 * a);
			if(x1.isInfinite() || x1.isNaN() || x2.isInfinite() || x2.isNaN()) {
				System.out.println("1");
			}else {
				if(x1 == x2) {
					System.out.println("1");
				}else {
					System.out.println("2");
				}				
			}
		} else {
			System.out.println("0");
		}
	}
}
